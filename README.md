# Supports de cours de l'école [JC2BIM](https://www.gdr-bim.cnrs.fr/ecole-jc2bim/). 

## Algorithmique des séquences

#### Cours et TP

* Alignement de séquences et graines: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/AlgoSeq/Cours/algoseq2021_graines_touzet.pdf)  

#### Ateliers

## Statistiques

#### Cours et TP

* Statistiques inférentielles: [supports [html]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/Stat_inferentielle/stats2021_StatsInferentielles_Mariadassou.html)
* Statistiques inférentielles (TP): [instructions d'installation](http://forgemia.inra.fr/guillem.rigaill/slides_jc2bim/-/tree/main/Stat/Cours/Stat_inferentielle_TP)
* Chaînes de Markov et Modèles de Markov: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/stats2021_MarkovHMM_Schbath.pdf)
* Statistiques bayésiennes: [supports [pdf]](http://guillem.rigaill.pages.mia.inra.fr/slides_jc2bim/Stat/Cours/Stat_bayesienne/source/bayes/stats2021_bayes_Mariadassou.pdf)

#### Ateliers
